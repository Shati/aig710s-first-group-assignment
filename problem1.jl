### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ f9791895-ee30-4b2a-93cc-96cbbe3167b4
using Markdown, InteractiveUtils, Pkg, DataStructures 

# ╔═╡ ac0aeec9-e7a8-41f6-bd05-7605d101736f
Pkg.add("DataStructures")

# ╔═╡ df89151e-9b95-4590-9dfe-fba0e791966c
struct Mission
	name::String
	cost::Int64
end

# ╔═╡ 315a0a0b-b4aa-42a6-9973-51b5a5e25319
struct Environment
	name::String
	position::Int64
	number_of_dirt::Vector{Int}
end

# ╔═╡ 77bfddcf-66f6-42bd-b2b2-69eb97a4552d
M1 = Mission("me", 3)

# ╔═╡ d9e88c17-ab1e-4758-9c8d-f02c44bc7da8
M2 = Mission("mw", 3)

# ╔═╡ 952a0c67-762a-4c19-8c00-cad35e7e4d46
M3 = Mission("re", 1)

# ╔═╡ 2c275c9b-4bab-40fe-b167-ac66d46a6654
M4 = Mission("co", 5)

# ╔═╡ 6dc99d8b-6129-41df-afde-a1bd518b27cd
Env1 = Environment("Environment One", 1, [1, 3, 2, 1])

# ╔═╡ 03e17099-f238-416b-9f6f-550b79e1adbe
Env2 = Environment("Environment Two", 2, [1, 3, 2, 1])

# ╔═╡ 4840cf15-e7bc-4c3f-8e14-80d1b50ec7fe
Env3 = Environment("Environment Three", 3, [1, 3, 2, 1])

# ╔═╡ 3eafa6fc-cee1-49ba-8633-896eab01e74a
Env4 = Environment("Environment Four", 4, [1, 3, 2, 1])

# ╔═╡ bbc97db0-4d7f-4596-ad32-02d2c00d8ab8
Env5 = Environment("Environment Five", 1, [0, 3, 2, 1])

# ╔═╡ 086616cc-0dcc-472f-b5e2-616360cee108
Env6 = Environment("Environment Six", 2, [0, 3, 2, 1])

# ╔═╡ f101919e-2bdd-44f5-897c-b6dc28af895d
Env7 = Environment("Environment Seven", 3, [0, 3, 2, 1])

# ╔═╡ 8ec9bfad-ccde-4e7b-9527-af4bcae7cd65
Env8 = Environment("Environment Eight", 4, [0, 3, 2, 1])

# ╔═╡ 4e6d0c0a-ab48-42d2-ab38-9d37fd442197
Env9 = Environment("Environment Nine", 4, [0, 3, 2, 0])

# ╔═╡ 102f81f8-6f2c-4ad7-92ad-62ca0282c5f2
TransModel = Dict()

# ╔═╡ 4b7c1edb-1dda-4123-94e8-2a1d0aa7886c
push!(TransModel, Env1 => [(M2, Env2), (M1, Env1), (M3, Env1), (M4, Env5)])

# ╔═╡ 79d9c702-8681-4b64-8b81-56ee27626c1e
push!(TransModel, Env2 => [(M2, Env3), (M1, Env1), (M3, Env2)])

# ╔═╡ 20242771-4ab2-4178-861a-e042d2eefbe5
push!(TransModel, Env3 => [(M2, Env4), (M1, Env2), (M3, Env3)])

# ╔═╡ 63e64f2e-f927-47e2-9e2a-8319590bc908
push!(TransModel, Env4 => [(M2, Env4), (M1, Env3), (M3, Env4)])

# ╔═╡ 3768a90e-5d65-4705-9945-6393875e852c
push!(TransModel, Env5 => [( M1, Env5), (M2, Env6), (M3, Env5)])

# ╔═╡ 8f3cd1e3-6445-499c-8713-1df453c74f20
push!(TransModel, Env6 => [( M1, Env5), (M2, Env7), (M3, Env6)])

# ╔═╡ 8a8726ea-c0f6-47ee-80db-5b1fc6be8e05
push!(TransModel, Env7 => [( M1, Env6), (M2, Env8), (M3, Env7)])

# ╔═╡ 99458ae0-c525-444c-b522-ea85c9ba78c7
push!(TransModel, Env8 => [( M1, Env7), (M2, Env8), (M3, Env8), (M4, Env9)])

# ╔═╡ affcfe44-4bba-43c2-a6e3-34914257b1d8
TransModel

# ╔═╡ c0ed6331-71e3-42e6-a6c3-055685205e07
function generate_result(trans_model, ancestors, startup_state, goal_state)
	#move backward, goal state to the startup state
	#ends when you find the startup state
	result = []
	discover = goal_state
		while !(discover == startup_state)
				current_state_ancestor = ancestors[explorer]
				related_transitions = trans_model[current_state_ancestor]
			for single_trans in related_transitions
				if single_trans[2] == explorer
					push!(result, single_trans[1])
					break
				else
					continue
				end
			end
				discover = current_state_ancestor
		end
	return result
end

# ╔═╡ 14d40032-3f37-48d6-9d2e-641a6efdd803
function heuristic(room)
	#add all items left for collection
	sum = 0
	for j in 1:length(room)
		sum += room[j]
	end
  return sum
end

# ╔═╡ c4a1a2d6-50f2-48e7-a9e3-91e56b782f44
room = [1, 3, 2, 1]

# ╔═╡ 1ee6ba38-f9a0-4d3a-bd52-119e38e9aa64
heuristic(room)

# ╔═╡ 2096a891-29d4-4020-b8b1-95fa0d19833b
function goal_tester(current_state::Environment)
  return ! (current_state.number_of_dirt[1]==0 || current_state.number_of_dirt[4]==0)
end

# ╔═╡ 32bb07f0-2299-4162-9b06-d0e42697d4ae
function add_pqueue_ucs(queue::PriorityQueue{Environment, Int64}, state::Environment,
cost::Int64)
	enqueue!(queue, state, cost)
	return queue
end

# ╔═╡ 113c88ef-774a-4da3-b03a-7a253ffd0d75
function add_stack(stack::Stack{Environment}, state::Environment, cost::Int64)
	push!(stack, state)
	return stack
end

# ╔═╡ 0057f524-a4d0-4d16-a75a-ac93ea3a3a6c
function pop_queue(queue::Queue{Environment})
removed = dequeue!(queue)
		return (removed, queue)
end

# ╔═╡ 3eac1950-6877-4c25-abf9-ad1076ef3f2b
function pop_stack(stack::Stack{Environment})
	removed = pop!(stack)
	return (removed, stack)
end

# ╔═╡ 8a5cfcd7-341c-4d50-a89f-e49368f4e090
function pop_pqueue_ucs(queue::PriorityQueue{Environment, Int64})
	removed = dequeue!(queue)
	return (removed, queue)
end

# ╔═╡ fce404c6-1fac-40a3-b93e-ff21bacdff21
function astar_algorithm(startup_state, transition_dict, goal_state, all_candidates,
		add_candidate, remove_candidate)
		explored = []
		ancestors = Dict{Environment, State}()
	     total_cost = 0
		the_candidates = add_candidate(all_candidates, startup_state, 0)
		parent = startup_state
	
	
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			 current_state = t1
			 the_candidates = t2
				#managing the current state
			 push!(explored, current_state)
             candidates = transition_dict[current_state]
			 for single_candidate in candidates
			   println(the_candidates)
			#
					
				  if !(single_candidate[2] in explored)
					   push!(ancestors, single_candidate[2] => current_state)
					
					#return result
					if (single_candidate[2] in  goal_state)
							return create_result(transition_dict, ancestors,
							  startup_state, single_candidate[2])
					  else
						# 
							the_candidates = add_candidate(the_candidates,
							single_candidate[2], single_candidate[1].cost + heuristic(single_candidate[2].number_of_dirt))
					
					end
				  end
				
			  end
         end
     end
	println(the_candidates)
end


# ╔═╡ 390ead99-eeb4-4ac2-ba82-cfa4f2c46397
astar_algorithm(Env3, TransModel,[Env4],PriorityQueue{Environment,Int64}(Base.Order.Reverse), add_pqueue_ucs, pop_pqueue_ucs)

# ╔═╡ Cell order:
# ╠═f9791895-ee30-4b2a-93cc-96cbbe3167b4
# ╠═ac0aeec9-e7a8-41f6-bd05-7605d101736f
# ╠═df89151e-9b95-4590-9dfe-fba0e791966c
# ╠═315a0a0b-b4aa-42a6-9973-51b5a5e25319
# ╠═77bfddcf-66f6-42bd-b2b2-69eb97a4552d
# ╠═d9e88c17-ab1e-4758-9c8d-f02c44bc7da8
# ╠═952a0c67-762a-4c19-8c00-cad35e7e4d46
# ╠═2c275c9b-4bab-40fe-b167-ac66d46a6654
# ╠═6dc99d8b-6129-41df-afde-a1bd518b27cd
# ╠═03e17099-f238-416b-9f6f-550b79e1adbe
# ╠═4840cf15-e7bc-4c3f-8e14-80d1b50ec7fe
# ╠═3eafa6fc-cee1-49ba-8633-896eab01e74a
# ╠═bbc97db0-4d7f-4596-ad32-02d2c00d8ab8
# ╠═086616cc-0dcc-472f-b5e2-616360cee108
# ╠═f101919e-2bdd-44f5-897c-b6dc28af895d
# ╠═8ec9bfad-ccde-4e7b-9527-af4bcae7cd65
# ╠═4e6d0c0a-ab48-42d2-ab38-9d37fd442197
# ╠═102f81f8-6f2c-4ad7-92ad-62ca0282c5f2
# ╠═4b7c1edb-1dda-4123-94e8-2a1d0aa7886c
# ╠═79d9c702-8681-4b64-8b81-56ee27626c1e
# ╠═20242771-4ab2-4178-861a-e042d2eefbe5
# ╠═63e64f2e-f927-47e2-9e2a-8319590bc908
# ╠═3768a90e-5d65-4705-9945-6393875e852c
# ╠═8f3cd1e3-6445-499c-8713-1df453c74f20
# ╠═8a8726ea-c0f6-47ee-80db-5b1fc6be8e05
# ╠═99458ae0-c525-444c-b522-ea85c9ba78c7
# ╠═affcfe44-4bba-43c2-a6e3-34914257b1d8
# ╠═c0ed6331-71e3-42e6-a6c3-055685205e07
# ╠═14d40032-3f37-48d6-9d2e-641a6efdd803
# ╠═c4a1a2d6-50f2-48e7-a9e3-91e56b782f44
# ╠═1ee6ba38-f9a0-4d3a-bd52-119e38e9aa64
# ╠═2096a891-29d4-4020-b8b1-95fa0d19833b
# ╠═32bb07f0-2299-4162-9b06-d0e42697d4ae
# ╠═113c88ef-774a-4da3-b03a-7a253ffd0d75
# ╠═0057f524-a4d0-4d16-a75a-ac93ea3a3a6c
# ╠═3eac1950-6877-4c25-abf9-ad1076ef3f2b
# ╠═8a5cfcd7-341c-4d50-a89f-e49368f4e090
# ╠═fce404c6-1fac-40a3-b93e-ff21bacdff21
# ╠═390ead99-eeb4-4ac2-ba82-cfa4f2c46397
