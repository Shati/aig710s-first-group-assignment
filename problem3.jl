### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 1d355740-b415-11eb-1369-0d26ac953e56
using Markdown, InteractiveUtils, Pkg 

# ╔═╡ 15042cf1-4256-4876-ac2a-8d49a85edcf2
Pkg.activate("Project.toml")

# ╔═╡ 70159503-e6ac-4ac7-bda1-352508b8329f
@enum ColourZone One Two Three Four

# ╔═╡ f3f1ff20-f653-4bc4-b8db-b39da5a5a2bc
ColourZone

# ╔═╡ 59cc013a-0558-442e-be4b-9af95670e626
mutable struct CSPVar
name::String

value::Union{Nothing,ColourZone}
forbidden_values::Vector{ColourZone}
domain_restriction_count::Int64
end

# ╔═╡ c6c88d09-5773-4bd6-95c2-f4ad6dacfb7a
rand(setdiff(Set([1,2,3, 4]),Set([2,3,4])))

# ╔═╡ 3b9bdfd7-8cd4-4e2d-919c-4263f37c4b43
struct ColourCSP
vars::Vector{CSPVar}
constraints::Vector{Tuple{CSPVar,CSPVar}}
end

# ╔═╡ 24738f9a-03ed-4471-a33f-d382fb81ae0c
function solve_csp(pb::ColourCSP, all_assignments)
		for cur_var in pb.vars
			if cur_var.domain_restriction_count == 4
				return []
			else
				next_val = rand(setdiff(Set([one,two,three,four]),
Set(cur_var.forbidden_values)))

					
			    println(cur_var.forbidden_values)
				  cur_var.value = next_val
			  
			
				for cur_constraint in pb.constraints
					if !((cur_constraint[1] == cur_var) || (cur_constraint[2] ==
							cur_var))
						continue
					  else
					if cur_constraint[1] == cur_var
								push!(cur_constraint[2].forbidden_values, next_val)
								cur_constraint[2].domain_restriction_count += 1
						e
                    else
                      push!(cur_constraint[1].forbidden_values, next_val)
                       cur_constraint[1].domain_restriction_count += 1
                     

		          end
              end
          end
       
      push!(all_assignments, cur_var.name => next_val)
      end
   end
return all_assignments
end


# ╔═╡ 7a99d4fa-7005-4d43-8fe5-93986f4a5331
x1 = CSPVar("X1",nothing, [], 0)

# ╔═╡ eb788efc-5cb1-40d9-b339-cd878c382190
x2 = CSPVar("X2", nothing, [], 0)

# ╔═╡ ac29affe-6b28-4083-aa15-cbb329724fed
x3 = CSPVar("X3", nothing, [Two, Three, Four], 0)

# ╔═╡ 7042d15c-3dc6-41a7-8634-25c52e7e3da8
x4 = CSPVar("X4", nothing, [], 0)

# ╔═╡ f9e760a6-be2e-4607-ad53-171fd0b44893
x5 = CSPVar("X5", nothing, [], 0)

# ╔═╡ 3b2734d6-a437-420f-8cd2-5d646355dfe3
x6 = CSPVar("X6", nothing, [], 0)

# ╔═╡ be7ef3d7-219d-48a1-b741-95379a7b673b
x7 = CSPVar("X7", nothing, [], 0)

# ╔═╡ 715e1115-a9be-4718-8682-67e95e4d2906
problem = ColourCSP([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])

# ╔═╡ 78b929e1-1b3a-44b4-a5d4-bf7df5132a31
solve_csp(problem, [])

# ╔═╡ Cell order:
# ╠═1d355740-b415-11eb-1369-0d26ac953e56
# ╠═15042cf1-4256-4876-ac2a-8d49a85edcf2
# ╠═70159503-e6ac-4ac7-bda1-352508b8329f
# ╠═f3f1ff20-f653-4bc4-b8db-b39da5a5a2bc
# ╠═59cc013a-0558-442e-be4b-9af95670e626
# ╠═c6c88d09-5773-4bd6-95c2-f4ad6dacfb7a
# ╠═3b9bdfd7-8cd4-4e2d-919c-4263f37c4b43
# ╠═24738f9a-03ed-4471-a33f-d382fb81ae0c
# ╠═7a99d4fa-7005-4d43-8fe5-93986f4a5331
# ╠═eb788efc-5cb1-40d9-b339-cd878c382190
# ╠═ac29affe-6b28-4083-aa15-cbb329724fed
# ╠═7042d15c-3dc6-41a7-8634-25c52e7e3da8
# ╠═f9e760a6-be2e-4607-ad53-171fd0b44893
# ╠═3b2734d6-a437-420f-8cd2-5d646355dfe3
# ╠═be7ef3d7-219d-48a1-b741-95379a7b673b
# ╠═715e1115-a9be-4718-8682-67e95e4d2906
# ╠═78b929e1-1b3a-44b4-a5d4-bf7df5132a31
